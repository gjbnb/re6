import numpy as np
import pandas as pd
import pickle
import logging
from imutils import paths   # 导入imutils中的paths工具,用于获取文件路径
from sklearn.model_selection import train_test_split
from lazypredict.Supervised import LazyClassifier

import logging  # 用于记录日志

#加载faiss_dog_cat_question生成好的X，y
with open('X.pkl', 'rb') as f1:
    X = np.array(pickle.load(f1))
with open('y.pkl', 'rb') as f2:
    y = np.array(pickle.load(f2))

# 将数据集分为训练集和测试集，测试集占比50%
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.5, random_state=123)

# 使用LazyClassifier自动选择和评估各种分类器
clf = LazyClassifier()
result, _ = clf.fit(X_train, X_test, y_train, y_test)
print(result)

# 获取F1分数最高的模型
best_model_name = result['F1 Score'].idxmax()  # 获取F1分数最高行的索引值，即：模型名称
print("\nF1分数最高的模型是: ", best_model_name)

# clf.models 是包含所有训练过的模型 (名称, 模型对象) 键值对的字典
best_model = clf.models[best_model_name]  # 根据模型名称，从模型字典中获取模型对象
result=best_model.predict(X_test)      #使用最佳分类器进行预测
with open("best_model.pkl",'wb') as f:
    pickle.dump(best_model,f)   #将最佳分类器保存到 best_model.pkl 文件中
    logging.info("最佳模型已保存在best_model.pkl文件中。")  # 记录日志

result = best_model.predict(X_test)  # 该字典可以直接被拿来进行预测
print(f"用{best_model_name}预测X_test的结果是:\n{result}")